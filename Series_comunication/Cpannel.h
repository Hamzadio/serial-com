#ifndef CPANNEL_H
#define CPANNEL_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>

#include <string>
using namespace std;


class CPannel : public QObject
{


    Q_OBJECT
public:

    explicit CPannel(QObject *parent = nullptr);

    unsigned int Affichemessagefixe(); //show only one thing
    unsigned int Affichemessage(string phrase);


private:

    QSerialPort Port;

    string calcul_LRC (string phrase);





};

#endif // CPANNEL_H
