﻿#include "Cpannel.h"

CPannel::CPannel(QObject *parent)
    : QObject{parent}
{

    Port.setBaudRate(QSerialPort::Baud9600); //speed transfer
    Port.setDataBits(QSerialPort::Data8); // date bytes set
    Port.setParity(QSerialPort::NoParity); // Parity of bytes
    Port.setStopBits(QSerialPort::OneStop); //Stop byte
    Port.setFlowControl(QSerialPort::NoFlowControl); // Control

    QString PortName;
    PortName = QSerialPortInfo::availablePorts().at(0).portName();
    Port.setPortName(PortName); // Name of the Port used

    Port.open(QIODevice::ReadWrite);
}
unsigned int CPannel::Affichemessage(std::string phrase)
{
    unsigned int resultat = 0; // 0 if everything is ok

    if (Port.isOpen())
    {

        string idtrame = "<ID01>";
        string debuddetrame = "<L1><PA><FA><Ma><WC><FA><CA>";
        string findetrame = "<E>";
        string messagecontenu = debuddetrame + phrase;

        unsigned char LRC = 0;
        for (unsigned int i = 0; i < messagecontenu.size(); ++i) {
            LRC = LRC ^ debuddetrame[i]; // i prend par valeur la longuer de la taille de la pharse { ex: Hello = 4  i =4}
        }
                                         //check the start to the "size -1" so 10-1 = 9 characters
                                         // OU exclusif {LRC = 0} et {debuddetrame = ?} les deux sont pris en binaire, puis on fait un exclusif
                                         // puis on fait un exclusif 0 + 0 = 0 ;  1 + 1 = 0  1 + 0 = 1
                                         //  1 0 1 0  +  1 1 0 1  =  0 1 1 1




        char LRCchaine[3];  //LRC base hexa; la valeur hexa est convertie en char {ex: 0x35 --> '35'} dans le [3] il y a le (3) le (5) et (le zero slash)

        sprintf(LRCchaine, "%02x", LRC ); //value of LRC in hex anc covert it to string

        string trame = idtrame + messagecontenu +LRCchaine + findetrame;


        Port.write(trame.c_str()); //  .c_str() memory zone of the caracters
        Port.waitForBytesWritten();

        QString response;
        while (Port.waitForReadyRead(2500)) //if we get a response
        {
            response += Port.readAll();
        }
            if (response == "NACK")
            {
                resultat = 1;
            }
            else

            if (response != "ACK")
            {
                resultat = 3; // nothing is received
            }

    }


    else
    {
        resultat = 2;
    }

    return resultat;
}



unsigned int CPannel::Affichemessagefixe()
{
    unsigned int resultat = 0; // 0 if everything is ok

    if (Port.isOpen())
    {
        Port.write("<ID01><L1><PA><FA><Ma><WC><FA><CA>IRIS55<E>");
        Port.waitForBytesWritten();

        QString response;
        while (Port.waitForReadyRead(2500)) //if we get a response
        {
            response += Port.readAll();
        }
            if (response == "NACK")
            {
                resultat = 1;
            }
            else

            if (response != "ACK")
            {
                resultat = 3; // nothing is received
            }

    }


    else
    {
        resultat = 2;
    }

    return resultat;
}
