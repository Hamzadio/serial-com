#ifndef CAPTUREDETRAME_H
#define CAPTUREDETRAME_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>

#include <iostream>
#include <string>
using namespace std;


class Capteur : public QObject
{


    Q_OBJECT
public:

    explicit Capteur(QObject *parent = nullptr);

    float Temperature;

    float VitesseVent;

    string CatchTrame();

private:

    QSerialPort Port;

    string calcul_LRC (string phrase);





};

#endif // CAPTUREDETRAME_H
